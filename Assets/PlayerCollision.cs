﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    [SerializeField] GateMechanic gateMechanic;
    public bool is1stGate = false;

    private void Start()
    {
        gateMechanic = FindObjectOfType<GateMechanic>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == gateMechanic.Gates[0])
        {
            is1stGate = true;
        }
    }
}
