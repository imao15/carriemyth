﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandDestoryer : MonoBehaviour
{
    public DynamicHand boolCheck;
    public BoxCollider box;

    public bool check = false;

    private void Awake()
    {
        boolCheck = FindObjectOfType<DynamicHand>();
        box = GetComponent<BoxCollider>();
    }
    private void FixedUpdate()
    {
        if (boolCheck.hasRisen == true)
        {
            check = true;
            Destroy(GetComponent<BoxCollider>());
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Objects")
        {
            Destroy(gameObject);
        }
    }
}
