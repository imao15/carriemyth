﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float distance = 170f;
    [SerializeField] float speedmultiplier = 1;
    public bool canMove = false;
    Vector3 posStart;

    private void Start()
    {
        posStart = transform.position;
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            InitiateMovement();
        }
    }

    private void InitiateMovement()
    {
        Vector3 posRaw = transform.position;

        if (posRaw.z < distance / 2 + posStart.z)
        {
            gameObject.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * speedmultiplier);
        }
        else
        {
            if (posRaw.z < distance + posStart.z)
            {
                gameObject.GetComponent<Rigidbody>().AddRelativeForce(-Vector3.forward * speedmultiplier);
            }
            else
            {
                Destroy(gameObject.GetComponent<Rigidbody>());
                Destroy(this);
            }
        }
    }
}
