﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicHand : MonoBehaviour
{
    [Header("References")]
    EnvironmentGenerator generator;
    PlayerCollision playerCollision;
    public List<GameObject> hands = new List<GameObject>();

    [Header("Randomizer")]
    [SerializeField] float baseHeightDiff = 18f;
    [SerializeField] float rotationRise = 20f;
    Vector2 speedRandomizer = new Vector2(6f, 10f);

    [Header("State Check")]
    public bool hasRisen = false;

    [Header("Lists")]
    List<float> speedRandomizers = new List<float>(); //makeprivate
    List<float> heightDiff = new List<float>(); //makeprivate
    List<float> yPosStarts = new List<float>(); //makeprivate
    List<float> yRotStarts = new List<float>(); //makeprivate
    List<float> lerps = new List<float>();

    void Start()
    {
        generator = GetComponent<EnvironmentGenerator>();
        playerCollision = FindObjectOfType<PlayerCollision>();
    }

    void Update()
    {
        if (generator.isInstanced)
        {
            generator.CleanList(hands);
            SetRise();
            generator.isInstanced = false;
        }

        if (playerCollision.is1stGate)
        {
            generator.EnableList(hands);
            hasRisen = true;
            playerCollision.is1stGate = false;
        }

        if (hasRisen)
        {
            InitiateRise();
        }
    }

    private void SetRise()
    {
        for (int i = 0; i < hands.Count; i++)
        {
            //GetInfo
            Vector3 posOld = hands[i].transform.position;
            Vector3 scaleOld = hands[i].transform.localScale;
            Vector3 rotOld = hands[i].transform.rotation.eulerAngles;

            //Create Height differences List
            float height = baseHeightDiff * scaleOld.y;
            heightDiff.Add(height);

            //Create yPos Start List
            float yPos = posOld.y - height;
            yPosStarts.Add(yPos);

            //create yRot Start List
            float yRot = rotOld.y;
            yRotStarts.Add(yRot);

            //Set new Position with lower height
            Vector3 posNew = new Vector3(posOld.x, yPos, posOld.z);
            hands[i].transform.position = posNew;

            //Create randomize speed list
            float speed = Random.Range(speedRandomizer.x, speedRandomizer.y);
            speedRandomizers.Add(speed);

            //Create Lerp List
            float lerp = 0;
            lerps.Add(lerp);
        }
    }

    void InitiateRise()
    {
        for (int i = 0; i < hands.Count; i++)
        {
            Vector3 posOld = hands[i].transform.position;
            Vector3 rotOld = hands[i].transform.rotation.eulerAngles;

            if (hands[i].transform.position.y < heightDiff[i] + yPosStarts[i])
            {
                //setup lerping
                lerps[i] = lerps[i] + 0.01f * speedRandomizers[i] * Time.deltaTime;
                //position difference
                float yPosChange = Mathf.Lerp(0, heightDiff[i], lerps[i]);
                float yPos = yPosStarts[i] + yPosChange;
                //rotation difference
                float yRotChange = Mathf.Lerp(0, rotationRise, lerps[i]);
                float yRot = yRotStarts[i] + yRotChange;
                
                //cgabge position
                Vector3 posNew = new Vector3(posOld.x, yPos, posOld.z);
                hands[i].transform.position = posNew;
                //chhange rotation
                Quaternion rotNew = Quaternion.Euler(rotOld.x, yRot, rotOld.z);
                hands[i].transform.rotation = rotNew;
            }
            else
            {
                hands[i].transform.position = posOld;
            }
        }
    }




    //yield return new WaitForSeconds(.5f);
    /*
    void AnimateHands()
    {
        foreach (GameObject hand in hands)
        {
            hand.transform.rotation = Quaternion.LookRotation(secondGate.position, Vector3.up);
        }
    }

        void RaiseHand()
    {
        for (int i = hands.Count - 1; i > -1; i--)
        {
            Vector3 posOld = hands[i].transform.position;

            //rawSpeeds[i]++;
            float Lerp = rawSpeeds[i] + 0.01f * speeds[i];
            rawSpeeds[i] = Lerp;
            if (rawSpeeds[i] < 1)
            {
                float yEnd = yStarts[i] + (newHeight * hands[i].transform.localScale.y);
                float yPos = Mathf.Lerp(yStarts[i], yEnd, rawSpeeds[i]);
                Vector3 posNew = new Vector3(posOld.x, yPos, posOld.z);
                hands[i].transform.position = posNew;
            }
            else
            {
                
            }
        }
    }
    */
}
