﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentGenerator : MonoBehaviour
{

    [Header("Randomizer")]
    [SerializeField] int instancedAmount = 500;
    [SerializeField] int XZPositionMultiplier = 100;
    [SerializeField] Vector2 YPositionRange = new Vector2(-20f,-1f);
    [SerializeField] Vector2 ScaleRange = new Vector2(0.75f, 3f);
    [SerializeField] Vector2 rotationX = new Vector2(-5f, 5f);
    [SerializeField] Vector2 rotationY = new Vector2(-360f, 360f);
    [SerializeField] Vector2 rotationZ = new Vector2(-5f, 5f);

    [Header("Hands")]
    [SerializeField] List<GameObject> objects = new List<GameObject>();
    //[SerializeField] GameObject hand = null;
    DynamicHand handDynamic;
    public bool isInstanced = false;

    GateMechanic gateMechanic;

    void Start()
    {
        //REFERENCE LOCAL!
        handDynamic = GetComponent<DynamicHand>();
        StartCoroutine(GenerateHands());
    }

    IEnumerator GenerateHands()
    {
        //Instantiate and creating List
        for (int i = 0; i < instancedAmount; i++)
        {
            Vector3 RandomPos = Random.insideUnitSphere * XZPositionMultiplier;
            //position
            float YPos = Random.Range(YPositionRange.x, YPositionRange.y);
            Vector3 NewPos = new Vector3(RandomPos.x, YPos, RandomPos.z);
            //rotation
            float x = Random.Range(rotationX.x, rotationX.y);
            float y = Random.Range(rotationY.x, rotationY.y);
            float z = Random.Range(rotationZ.x, rotationZ.y);
            Vector3 rotNew = new Vector3(x, y, z);
            //randomize Instantiate Object
            int randomIndex = 0;
            randomIndex = Random.Range(0, objects.Count - 1);
            //instantiateinspecifiedParent //todo mayve serialize parent
            GameObject instantiation = Instantiate(objects[randomIndex], NewPos, Quaternion.Euler(rotNew), gameObject.transform);
            //scale
            float scaleRange = Random.Range(ScaleRange.x, ScaleRange.y);
            instantiation.transform.localScale = new Vector3(scaleRange, scaleRange, scaleRange);
            //addlist
            if (handDynamic != null)
            {
                handDynamic.hands.Add(instantiation);
            }
            yield return null;
        }
        isInstanced = true;
    }

    public void CleanList(List<GameObject> lists)
    {
            for (int i = lists.Count - 1; i > -1; i--)
            {
                if (lists[i] == null)
                {
                    lists.RemoveAt(i);
                }
            }
    }

    public void EnableList(List<GameObject> lists)
    {
            for (int i = lists.Count - 1; i > -1; i--)
            {
                //Enable
                lists[i].transform.Find("Geo").gameObject.SetActive(true);
            }
    }
}

